import os

import tensorflow as tf

class Pipeline(object):
  def __init__(self, sequence_length):
    self.sequence_length = sequence_length

  def _read_tfrecord(self, filename_queue):
    """Reads and parses an example from filename_queue.

    Arguments:
      filename_queue: A queue of strings with filenames to read from.

    Returns:
      phrase: sequence of word ids composing a phrase [sequence_length].
      sentiment: integer representing the sentiment for the phrase.
    """
    reader = tf.TFRecordReader()
    k, serialized_example = reader.read(filename_queue)
    out = tf.parse_single_example(
        serialized_example,
        features={
          "phrase": tf.FixedLenFeature([self.sequence_length], tf.int64),
          "sentiment": tf.FixedLenFeature([], tf.int64),
        })
    return out["phrase"], out["sentiment"]
    
    
  def tfrecord_input(self, filenames, min_queue_examples, batch_size, num_threads=1, shuffle=True):
    """Construct input for model using Reader ops.

    Arguments:
      filenames: List of datafiles to process.
      batch_size: Number of examples per batch.
      min_queue_examples: Minimum number of samples to retain in the batching
        queue.
      num_threads: Number of enqueuing threads.
      shuffle: Incates whether to use a shuffling queue.

    Returns:
      x: 2-D tensor of vocab_processed phrases [batch_size, sequence_length]
      y: 2-D tensor of integer labels [batch_size,]
    """
    for f in filenames:
      if not tf.gfile.Exists(f):
        raise ValueError("Failed to find file: {}".format(f))

    filename_queue = tf.train.string_input_producer(filenames)
    x, y = self._read_tfrecord(filename_queue)
    if shuffle:
      x_batch, y_batch = tf.train.shuffle_batch(
        [x, y],
        batch_size=batch_size,
        capacity=min_queue_examples + 3 * batch_size,
        min_after_dequeue=min_queue_examples,
        num_threads=num_threads)
    else:
      x_batch, y_batch = tf.train.batch(
        [x, y],
        batch_size=batch_size,
        capacity=min_queue_examples + 3 * batch_size,
        min_after_dequeue=min_queue_examples,
        num_threads=num_threads)
    return x_batch, y_batch
