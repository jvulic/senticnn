import argparse
import glob
import logging
import math
import time
import os
import sys

import tensorflow as tf

from s_model import SentimentModel
from s_model import  MOVING_AVERAGE_DECAY
import s_input

log = logging.getLogger("tensorflow")
FLAGS = tf.app.flags.FLAGS

# Evaluation flags.
tf.app.flags.DEFINE_integer("eval-interval", 60*1, "Evaluation interval in seconds")
tf.app.flags.DEFINE_integer("num-examples", 4096, "Number of examples to run each evaluation cycle")
tf.app.flags.DEFINE_boolean("run-once", False, "Whether to run evaluation only once")

# Input flags.
tf.app.flags.DEFINE_integer("min-queue-examples", 128, "Minimum number of samples to retain in the batching queue")
tf.app.flags.DEFINE_integer("batch-size", 64, "Training batch size")
tf.app.flags.DEFINE_integer("num-threads", 8, "Number of threads managing the input pipeline")

# Misc flags.
tf.app.flags.DEFINE_boolean("allow-soft-placement", True, "Allow soft device placement")
tf.app.flags.DEFINE_boolean("log-device-placement", False, "Log placement of ops on devices")
tf.app.flags.DEFINE_string("log-level", "INFO", "Set the logging level")

def eval_once(checkpoint_dir, saver, accuracy_op, summary_op, summary_writer):
  """Run evaluation on the model once."""
  with tf.Session() as sess:
    ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
    if ckpt and ckpt.model_checkpoint_path:
      # Restore from the checkpoint.
      saver.restore(sess, ckpt.model_checkpoint_path)
      # Assuming model_checkpoint_path looks something like:
      #   /run/train/model.ckpt-0,
      # extract global_step from it.
      global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
      #global_step = tf.train.get_global_step()
    else:
      log.Error("No checkpoint file found")
      return

    # Start the queue runners.
    coord = tf.train.Coordinator()
    try:
      threads = []
      for qr in tf.get_collection(tf.GraphKeys.QUEUE_RUNNERS):
        threads.extend(qr.create_threads(sess, coord=coord, daemon=True, start=True))

      num_iter = int(math.ceil(FLAGS.num_examples / FLAGS.batch_size))
      itr = 0
      while  itr < num_iter and not coord.should_stop():
        accuracy, summary = sess.run([accuracy_op, summary_op])
        log.info("{}: step {} acc {}".format(itr, global_step, accuracy))
        summary_writer.add_summary(summary, global_step)
        itr += 1
    except Exception as e:
      coord.request_stop(e)

    coord.request_stop()
    coord.join(threads, stop_grace_period_secs=10)

def evaluate(checkpoint_dir, model, eval_dir, filenames):
  """Evaluate the Sentiment model."""
  with tf.Graph().as_default() as g:
    x, y = s_input.Pipeline(
        model.sequence_length,
    ).tfrecord_input(
        filenames,
        FLAGS.min_queue_examples,
        FLAGS.batch_size,
        num_threads=FLAGS.num_threads)
    logit_op = model.inference(x, dropout_prob=0)
    accuracy_op = model.accuracy(model.prediction(logit_op), y)

    # Restore the moving average version of the learned variables.
    variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY)
    variables_to_restore = variable_averages.variables_to_restore()
    saver = tf.train.Saver(variables_to_restore)

    summary_op = tf.summary.merge_all()
    summary_writer = tf.summary.FileWriter(eval_dir, g)
    
    while True:
      eval_once(checkpoint_dir, saver, accuracy_op, summary_op, summary_writer)
      if FLAGS.run_once:
        break
      time.sleep(FLAGS.eval_interval)

def main(argv=None):
  class HelpArgumentParser(argparse.ArgumentParser):
    def error(self, message):
      sys.stderr.write("error: {}\n".format(message))
      self.print_help(sys.stderr)
      sys.exit(2)

  log.setLevel(FLAGS.log_level)
  parser = HelpArgumentParser(add_help=False)
  parser.add_argument(
      "run_dir",
      type=str,
      help="Directory of the run to evaluate")
  parser.add_argument(
      "data_dir",
      type=str,
      help="Path to data directory: /data/{vocab, train/, eval/}")
  args, _ = parser.parse_known_args()
  if not os.path.exists(args.data_dir):
    log.error("Data directory does not exist: {}".format(args.data_dir))
    return

  checkpoint_dir = os.path.join(args.run_dir, "train")
  if not os.path.exists(checkpoint_dir):
    log.error("Checkpoint directory not found: {}".format(checkpoint_dir))
    return
  model_filepath = os.path.join(checkpoint_dir, "model.pkl")
  if not os.path.exists(model_filepath):
    log.error("Model file not found: {}".format(model_filepath))
    return
  model = SentimentModel.restore(model_filepath)
  eval_dir = os.path.join(args.run_dir, "eval")
  if not os.path.exists(eval_dir):
    os.makedirs(eval_dir)
  # Fetch filenames from the eval directory under data.
  filenames = glob.glob(os.path.join(args.data_dir, "eval", "*.tfrecord"))

  evaluate(checkpoint_dir, model, eval_dir, filenames)

if __name__ == "__main__":
  tf.app.run(main=main, argv=sys.argv)
