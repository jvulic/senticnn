# Senticnn
Convolution Neural Net approach to sentiment extraction from text.

## Inspiration
I owe much of the ideas behind this implementation to the wonderful tutorials
available for TensorFlow (in particular the [CNN
tutorial](https://www.tensorflow.org/tutorials/deep_cnn/)) and the wonderful
writeup of [CNNs for
NLP](http://www.wildml.com/2015/11/understanding-convolutional-neural-networks-for-nlp/)
that really helped cement my understanding in a terse way while providing a
plethora of paper references for further in-depth study.

## Install
0. Install `python3.5`
1. Create virtual environment with python module `venv`:
`python3 -m venv $(pwd)`
2. Activate the environment:
`source ./bin/activate`
3. Install TensorFlow and all other required modules:
`pip install -r requirements.txt`

## File Overview
File | What's in it?
--- | ---
`s_model.py` | The model definition used to create and customize the computation graph.
`s_input.py` | Methods to setup the input pipeline of `.tfrecord`.
`s_train.py` | Defines and runs training on the model.
`s_eval.py` | Defines and runs evaluation on trained model.
`s_infer.py` | A fun hook to basically allow arbitrary inference of text using trained models.

## Usage
### python3 s_train.py
`s_train.py` is the main script used to train new models, or continue training
from checkpoints.
```
# Train a new model using training data from `data/rotten`.
python3 s_train.py data/rotten/

# Train the model under run `runs/12345` using training data from `data/rotten`.
python3 s_train.py data/rotten/ --run-dir=runs/12345
```

### python3 s_eval.py
`s_eval.py` performs evaluation on a trained model using the evaluation
dataset.
```
# Run evaluation on the model under `runs/12345` using evaluation data from
# `data/rotten`.
python3 s_eval.py runs/12345 data/rotten 
```

### python3 s_infer.py
`s_infer.py` provides a means to run arbitrary strings through a trained model.
Note that is uses the vocabulary specified so the provided string needs
to adhere to any sentence format rules (expanded contractions etc.) as the
sentence is not cleaned by the script.
```
# Run inference on the given phrase using the model under `run/12345` using the
# same data setup as `data/rotten` (effectively the same vocabulary).
python3 s_infer.py runs/12345 data/rotten "I hate this movie so much"
```

## Data
### Provided
Nothing crazy, a small dataset taken from
[kaggle](https://www.kaggle.com/c/sentiment-analysis-on-movie-reviews) based on
[rottentomates](https://www.rottentomatoes.com/) movie reviews. Originally the
dataset is provided as a serialized parse tree for sentences from various movie
reviews, but I only use the root of the parse tree (the complete sentence) in
training and validation. This yields a total of `8529` classified sentences to
work with. Enough to have some fun with.

### Custom
See the provided data directory for an example `data/rotten`. Basically, any
dataset you wish you include you will need to use a common word to word-id
vocabulary and convert the data to a `.tfrecord` with the common encoding. Then
just place the data within either `train` or `eval` to augment the training or
validation datasets.

## Performance
Despite the incredibly small dataset and of `6397` training examples and `2132`
evaluation examples, rampant over-fitting despite over-fitting avoidance methods
such as `dropout` and `l2 regularization`, lack of some more involved methods
of learning such as ensembles, or cross-validation, and fairly simple
classification approach in terms of network construction; This outperforms
random guessing, pseudo-random guessing (most phrases I inherently more likely
to be neutral) and even outperformed my Naive Bayes implementation that used a
bag-of-words (word counts) approach.

The network achieves `~60%` correct classification on the evaluation dataset
under `3-class` sentiment problem of `0=negative`, `1=neutral`, `2=positive`.
Nothing to write home about, but considering the simple approach it definitely
can act as a starting point for more involved constructions.

That said, you can still have some fun with it. Even after a measly 2500
learning steps it is able to classify (subjectively) correctly some obvious and
perhaps not so obvious examples:

```
# Negative.
python3 s_infer.py runs/1484615944 data/rotten/ "You are so stupid I hate you"
INFO:tensorflow:logits [[ 3.66708446 -2.19280767 -1.49567246]] prediction [0]

# Positive.
python3 s_infer.py runs/1484615944 data/rotten/ "You are so great I love you"
INFO:tensorflow:logits [[-1.60088086 -0.11105418  1.70264935]] prediction [2]

# Neutral.
python3 s_infer.py runs/1484615944 data/rotten/ "You are okay"               
INFO:tensorflow:logits [[-0.82194436  1.1482271  -0.34067461]] prediction [1]

# Positive by a small margin.
python3 s_infer.py runs/1484615944 data/rotten/ "You are so great I hate you"
INFO:tensorflow:logits [[-0.24201751  0.00084412  0.22796482]] prediction [2]
```

In particular, looking at the fourth example, we can see an argument of how the
message could be deemed as `positive`. However, we can get a more complete
picture by looking at the raw Looking at the raw output from the logistic
regression step. Though I did not perform softmax on the output, we can still
grasp at an inkling of how confident the prediction. If anything, its another
interesting piece of information to consider. To note, its generally more
difficult to correctly classify short examples due to the lack of a more
complete context and consequently stronger reliance on individual words for
sentiment.

## TensorBoard
Its fun and useful to visualize performance of the model from something other
than logging information from stdout. While training, the model generates
summaries of various internal values which can be tracked and visualized using
`tensorboard`. It additionally provides a incredibly useful visualization of
the graph itself, and even a means of visualizing the word embeddings through
use of PCA or T-SNE!

Provided is a dead-simple hook to starting TensorBoard:
```
# Run tensorboard displaying data for the model under `runs/12345`
./board /runs/12345
```

### Pretty Pictures
#### Graph
Below is the data-flow graph created when training the model.  
![tensorflow-graph](https://s28.postimg.org/yzzdkeze5/tf_graph.png)

#### Embedding
The embedding it particularly interesting as we can see three distinct clusters
-- seemingly one cluster for each sentiment class.  
![tensorflow-embedding-gen](https://s29.postimg.org/mofhv7xd3/embedding_general.png)

Upon inspection is coming pretty apparent that our suspicion may be true as
similar sentiment words seemed to be clustered closer together. Below is an
example of the left-most cluster which seems to be grouping negative sentiment.  
![tensorflow-embedding-neg](https://s29.postimg.org/rash2a94n/embedding_negative.png)

## License
[MIT License](LICENSE)