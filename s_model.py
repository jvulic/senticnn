import pickle

import tensorflow as tf

# Training CONSTANTS.
MOVING_AVERAGE_DECAY = 0.9

def _add_loss_summaries(total_loss):
  """Add summaries for losses in the sentiment model.

  Arguments:
    total_loss: Total loss from loss()

  Returns:
    loss_averages_op: Op for generating moving averages of losses.
  """
  loss_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY, name="avg")
  losses = tf.get_collection("losses")
  loss_averages_op = loss_averages.apply(losses + [total_loss])

  # Attach a scalar summary to all individual losses and the total loss;
  # do the same for the averaged version of the losses.
  for l in losses + [total_loss]:
    tf.summary.scalar("{}_raw".format(l.op.name), l)
    tf.summary.scalar(l.op.name, loss_averages.average(l))
  return loss_averages_op

def _activation_summary(var):
  """Helper to create summaries for activations.

  Creates a summary that provides a histogram of activations and the
  sparsity of activations.

  Arguments:
    x: A tensor.
  
  Returns:
    Nothing.
  """
  tf.summary.histogram("{}/activations".format(var.op.name), var)
  tf.summary.histogram("{}/sparsity".format(var.op.name), tf.nn.zero_fraction(var))

def _variable_on_cpu(name, shape, initializer):
  """Helper to create a Variable stored on CPU memory.

  Arguments:
    name: Name of variable.
    shape: List of ints describing the shape.
    initializer: Initializer for the Variable.

  Returns:
    Variable tensor.
  """
  with tf.device("/cpu:0"):
    var = tf.get_variable(name, shape, initializer=initializer)
  return var

def _variable_with_weight_decay(name, shape, stddev, wd):
  """Helper to create an Variable with weight decay.

  Arguments:
    name: Name of the variable.
    shape: List of ints describing the shape.
    stddev: Standard deviation of a truncated normal (guassian).
    wd: L2 regularization lambda.

  Returns:
    Variable tensor.
  """
  var = _variable_on_cpu(
      name,
      shape,
      tf.truncated_normal_initializer(stddev=stddev))
  weight_decay = tf.mul(tf.nn.l2_loss(var), wd, name="weight_loss")
  tf.add_to_collection("losses", weight_decay)
  return var

class SentimentModel(object):
  def __init__(self,
      sequence_length,
      num_classes,
      vocab_size,
      embedding_size,
      filter_sizes,
      num_filters,
      l2_lambda):
    """Creates a new Sentiment model."""
    self.sequence_length = sequence_length
    self.num_classes = num_classes
    self.vocab_size = vocab_size
    self.embedding_size = embedding_size
    self.filter_sizes = filter_sizes
    self.num_filters = num_filters
    self.l2_lambda = l2_lambda

  def inference(self, x, dropout_prob=0):
    """Build the CNN-based textual sentiment model.
    
    embedding > convolution > concat > dropout > logits

    Arguments:
      x: 2-D phrase word-id tensors returned from inputs().
      dropout_prob: Probability to drop neurons at output layer.

    Returns:
      Logits tensor.
    """
    with tf.variable_scope("embedding") as scope:
      embedding = _variable_on_cpu(
          scope.name,
          [self.vocab_size, self.embedding_size],
          tf.random_uniform_initializer(-1.0, 1.0))
      tf.add_to_collection("embedding", embedding)
      # conv2d expects a 4-D tensor of [batch, width, height, channel], so to
      # to make our word embedding compatible we expand and introduce a
      # constant for the fourth channel.
      x_embedding = tf.nn.embedding_lookup(embedding, x)
      x_embedding_expanded = tf.expand_dims(x_embedding, -1) # -1 applies axisension to the end

    pool_outputs = []
    for filter_size in self.filter_sizes:
      with tf.variable_scope("conv{}".format(filter_size)) as scope:
        kernel = _variable_with_weight_decay(
            "weights{}".format(filter_size),
            [filter_size, self.embedding_size, 1, self.num_filters],
            stddev=0.1,
            wd=self.l2_lambda)
        conv = tf.nn.conv2d(
            x_embedding_expanded,
            kernel,
            strides=[1, 1, 1, 1],
            padding="VALID")
        bias = _variable_on_cpu(
            "bias{}".format(filter_size),
            [self.num_filters],
            tf.constant_initializer(0.1))
        activation = tf.nn.relu(tf.nn.bias_add(conv, bias), name=scope.name)
        _activation_summary(activation)
      with tf.variable_scope("pool{}".format(filter_size)) as scope:
        pool = tf.nn.max_pool(
            activation,
            ksize=[1, self.sequence_length-filter_size+1, 1, 1],
            strides=[1, 1, 1, 1],
            padding="VALID",
            name=scope.name)
      pool_outputs.append(pool)

    with tf.variable_scope("concat") as scope:
      num_filters_total = self.num_filters * len(self.filter_sizes)
      pools = tf.concat(3, pool_outputs)
      pools_flat = tf.reshape(pools, [-1, num_filters_total], name=scope.name)

    drop = tf.nn.dropout(pools_flat, 1-dropout_prob, name="dropout")
    with tf.variable_scope("logits_linear") as scope:
      W = _variable_with_weight_decay(
          "weights",
          [num_filters_total, self.num_classes],
          stddev=0.1,
          wd=self.l2_lambda)
      b = _variable_on_cpu(
          "bias",
          [self.num_classes],
          tf.constant_initializer(0.1))
      logits_linear = tf.add(tf.matmul(drop, W), b, name=scope.name)
    return logits_linear

  def prediction(self, logits):
    """Calculates the predictions based of the logits.

    Arguments:
      logits: Logits from inference().

    Returns:
      Predictions tensor.
    """
    return tf.argmax(logits, 1, name="predictions")

  def accuracy(self, predictions, y):
    """Calculate the batch accuracy.
      
    Arguments:
      predictions: Batch predictions from predictions().
      y: One-Hot sentiment labels from input().

    Returns:
      Accuracy tensor.
    """
    with tf.variable_scope("accuracy") as scope:
      correct_predictions = tf.equal(predictions, y)
      accuracy = tf.reduce_mean(
          tf.cast(correct_predictions, tf.float32),
          name=scope.name)
    tf.summary.scalar("accuracy", accuracy)
    return accuracy

  def loss(self, logits, y):
    """Add L2 Loss to all trainable variables.

    Arguments:
      logits: Logits from inference().
      y: One-Hot sentiment labels from input().

    Returns:
      Loss tensor.
    """
    with tf.variable_scope("cross_entropy") as scope:
      y_oh = tf.one_hot(y, self.num_classes)
      cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
          labels=y_oh, logits=logits, name="cross_entropy_per_example")
      cross_entropy_mean = tf.reduce_mean(cross_entropy, name=scope.name)
    tf.add_to_collection("losses", cross_entropy_mean)
    
    # total_loss = cross entropy + all weight decay (l2 loss)
    return tf.add_n(tf.get_collection("losses"), name="total_loss")

  def train(self, total_loss, global_step):
    """Train the CNN-based textual sentiment model.

    Arguments:
      total_loss: Total loss from loss().
      global_step: Integer variable counting the number of training steps
                   processed.
    
    Returns:
      train_op: Op for training.
    """
    loss_averages_op = _add_loss_summaries(total_loss)

    with tf.control_dependencies([loss_averages_op]):
      opt = tf.train.AdamOptimizer()
      grads = opt.compute_gradients(total_loss)

    apply_gradient_op = opt.apply_gradients(grads, global_step=global_step)

    # Add histograms for trainable variables.
    for var in tf.trainable_variables():
      tf.summary.histogram(var.op.name, var)

    # Add histograms for gradients.
    for grad, var in grads:
      if grad is not None:
        tf.summary.histogram("{}/gradients".format(var.op.name), grad)
        
    # Track the moving averages of all trainable variables.
    variable_averages = tf.train.ExponentialMovingAverage(
        MOVING_AVERAGE_DECAY, global_step)
    variables_averages_op = variable_averages.apply(tf.trainable_variables())

    with tf.control_dependencies([apply_gradient_op, variables_averages_op]):
      train_op = tf.no_op(name="train")
    return train_op

  def save(self, filename):
    """Saves the model into the given file.

    Arguments:
      filename: Path to output file.
    
    Returns:
      Nothing.
    """
    with open(filename, "wb") as f:
      pickle.dump(self, f)

  @classmethod
  def restore(cls, filename):
    """Restores model from given file.

    Arguments:
      filename: Path to file to load from.

    Returns:
      SentimentModel object.
    """
    with open(filename, "rb") as f:
      return pickle.load(f)


