import argparse
import csv
import os
from zipfile import ZipFile

import numpy as np
import tensorflow as tf
from tensorflow.contrib.learn.python.learn.preprocessing import VocabularyProcessor

"""
Natively the rotten dataset is provided as a parse tree with 5 sentiment
classes.  For our uses, we convert this down to 3 classes using only the root
of the parse tree (entire phrase valuation). 
"""

if __name__ == "__main__":
  _num_classes = 3
  def _map_down(sentiment):
    """Maps from 5 class sentiment to 3 class sentiment.

    Arguments:
      sentiment: sentiment class as a integer.
    Returns:
      New sentiment class.
    """
    switcher = {
      0: 0,
      1: 0,
      2: 1,
      3: 2,
      4: 2,
    }
    return switcher.get(sentiment)
  def _create_tfrecord(filename, data):
    """Creates a TFRecord file in the current directory.
    
    Arguments:
        data: tuple of (word-id phrase, one-hot sentiment tensor). 

    Returns:
      Nothing.
    """
    def _int64_feature(value):
      return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))
    def _int64_feature_list(value):
      return tf.train.Feature(int64_list=tf.train.Int64List(value=value))
    with tf.python_io.TFRecordWriter(filename) as writer:
      for entry in data:
        x, y = entry
        example = tf.train.Example(features=tf.train.Features(feature={
          "phrase": _int64_feature_list(x.tolist()),
          "sentiment": _int64_feature(y),
        }))
        writer.write(example.SerializeToString())

  parser = argparse.ArgumentParser(
      prog="convert",
      description="""Converts the dataset to a digestable TFRecord and
      Vocabulary to be processed by the sentiment model.""",
      usage="convert [options]")
  parser.add_argument("--max-sequence-length", type=int, default=None, help="Maximum word sequence length for input text")
  parser.add_argument("--min-word-freq", type=int, default=0, help="Minimum frequency of words in the vocabulary")
  parser.add_argument("--train-eval-split", type=float, default=0.25, help="Train/Evaluation data split percentage")
  args = parser.parse_args()

  ZipFile("raw/train.tsv.zip").extractall("raw")
  x_text, y = [], []
  with open("raw/train.tsv") as f:
    reader = csv.DictReader(f, delimiter="\t")
    # We de-dupe and remove rows with common SentenceIds as we do not need
    # parse tree information.
    sid = 0
    for row in reader:
      if sid == row["SentenceId"]:
          continue
      x_text.append(row["Phrase"])
      # Convert sentiment to one-hot tensor.
      y.append(_map_down(int(row["Sentiment"])))
      sid = row["SentenceId"]

  # Set sensible default for max_sequence_length.
  if not args.max_sequence_length:
      args.max_sequence_length = max([len(x.split(" ")) for x in x_text])

  # Create and save the vocabulary based off the dataset.
  vocab_processor = VocabularyProcessor(
      args.max_sequence_length,
      min_frequency=args.min_word_freq)
  x = list(vocab_processor.fit_transform(x_text))
  vocab_processor.save("vocab")

  split_idx = int(len(y)*args.train_eval_split)
  train_dir = "train"
  print("{} train examples".format(len(y[split_idx:])))
  if not os.path.exists(train_dir):
    os.makedirs(train_dir)
    filename = "{}-rotten_train.tfrecord".format(_num_classes)
    _create_tfrecord(os.path.join(train_dir, filename), zip(x[split_idx:], y[split_idx:]))

  eval_dir = "eval"
  print("{} evaluation examples".format(len(y[:split_idx])))
  if not os.path.exists(eval_dir):
    os.makedirs(eval_dir)
    filename = "{}-rotten_eval.tfrecord".format(_num_classes)
    _create_tfrecord(os.path.join(eval_dir, filename), zip(x[:split_idx], y[:split_idx]))

