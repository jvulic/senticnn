import argparse
import logging
import time
import os
import sys

import numpy as np
import tensorflow as tf
from tensorflow.contrib.learn.python.learn.preprocessing import VocabularyProcessor

from s_model import SentimentModel
from s_model import  MOVING_AVERAGE_DECAY

log = logging.getLogger("tensorflow")
FLAGS = tf.app.flags.FLAGS

# Inference flags.

# Input flags.

# Misc flags.
tf.app.flags.DEFINE_boolean("allow-soft-placement", True, "Allow soft device placement")
tf.app.flags.DEFINE_boolean("log-device-placement", False, "Log placement of ops on devices")
tf.app.flags.DEFINE_string("log-level", "INFO", "Set the logging level")

def infer(checkpoint_dir, model, x):
  """Analyze sentiment with the given model on the given phrase."""
  with tf.Graph().as_default() as g:
    logit_op = model.inference(x, dropout_prob=0)
    prediction_op = model.prediction(logit_op)
    
    # Restore the moving average version of the learned variables.
    variable_averages = tf.train.ExponentialMovingAverage(MOVING_AVERAGE_DECAY)
    variables_to_restore = variable_averages.variables_to_restore()
    saver = tf.train.Saver(variables_to_restore)

    with tf.Session() as sess:
      ckpt = tf.train.get_checkpoint_state(checkpoint_dir)
      if ckpt and ckpt.model_checkpoint_path:
        # Restore from the checkpoint.
        saver.restore(sess, ckpt.model_checkpoint_path)
      else:
        log.Error("No checkpoint file found")
        return

      logits, predictions = sess.run([logit_op, prediction_op])
      log.info("logits {} prediction {}".format(logits, predictions))

def main(argv=None):
  log.setLevel(FLAGS.log_level)

  class HelpArgumentParser(argparse.ArgumentParser):
    def error(self, message):
      sys.stderr.write("error: {}\n".format(message))
      self.print_help(sys.stderr)
      sys.exit(2)
  parser = HelpArgumentParser(add_help=False)
  parser.add_argument(
      "run_dir",
      type=str,
      help="Directory of the run to use for inference")
  parser.add_argument(
      "data_dir",
      type=str,
      help="Path to data directory: /data/{vocab, train/, eval/}")
  parser.add_argument(
      "phrase",
      type=str,
      help="Phrase to perform sentiment analysis on")
  args, _ = parser.parse_known_args()
  if not os.path.exists(args.data_dir):
    log.error("Data directory does not exist: {}".format(args.data_dir))
    return

  checkpoint_dir = os.path.join(args.run_dir, "train")
  if not os.path.exists(checkpoint_dir):
    log.error("Checkpoint directory not found: {}".format(checkpoint_dir))
    return
  model_filepath = os.path.join(checkpoint_dir, "model.pkl")
  if not os.path.exists(model_filepath):
    log.error("Model file not found: {}".format(model_filepath))
    return
  model = SentimentModel.restore(model_filepath)
  # Transform input phrase with vocabulary used by the model.
  vocab_processor = VocabularyProcessor.restore(os.path.join(args.data_dir, "vocab"))
  x = np.array(list(vocab_processor.transform([args.phrase])))

  infer(checkpoint_dir, model, x)

if __name__ == "__main__":
  tf.app.run(main=main, argv=sys.argv)
