import argparse
import glob
import threading
import logging
from datetime import datetime
import pickle
import time
import os
import sys

import tensorflow as tf
from tensorflow.contrib.learn.python.learn.preprocessing import VocabularyProcessor
#from tensorflow.python.platform import tf_logging as logging

from s_model import SentimentModel
import s_input

log = logging.getLogger("tensorflow")
FLAGS = tf.app.flags.FLAGS

# Model parameter flags.
tf.app.flags.DEFINE_integer("embedding-size", 128, "Word embedding size")
tf.app.flags.DEFINE_integer("num-classes", 3, "The number of sentiment classes")
tf.app.flags.DEFINE_string("filter-sizes", "3,4,5", "Comma seperated conv filter sizes")
tf.app.flags.DEFINE_integer("num-filters", 128, "Number of filters per filter size")
tf.app.flags.DEFINE_float("l2-lambda", 0.1, "L2 regularization lambda")
tf.app.flags.DEFINE_float("dropout-prob", 0.5, "Dropout probability")

# Training flags.
tf.app.flags.DEFINE_string("run-dir", "runs/{}".format(str(int(time.time()))),
    "Directory to store run data of the model")
tf.app.flags.DEFINE_integer("max-steps", 1000000, "Maximum number of batches to run")
tf.app.flags.DEFINE_integer("save-checkpoint-secs", 60*1, "Second interval to save checkpoints")
tf.app.flags.DEFINE_integer("save-summaries-steps", 10, "Step interval to save summaries")
tf.app.flags.DEFINE_integer("stat-interval", 5, "Steps interval for logging stats")
tf.app.flags.DEFINE_boolean("dump-embedding", False, "Dump the embedding matrix")

# Input flags.
tf.app.flags.DEFINE_integer("min-queue-examples", 128, "Minimum number of samples to retain in the batching queue")
tf.app.flags.DEFINE_integer("batch-size", 64, "Training batch size")
tf.app.flags.DEFINE_integer("num-threads", 8, "Number of threads managing the input pipeline")

# Misc flags.
tf.app.flags.DEFINE_boolean("allow-soft-placement", True, "Allow soft device placement")
tf.app.flags.DEFINE_boolean("log-device-placement", False, "Log placement of ops on devices")
tf.app.flags.DEFINE_string("log-level", "INFO", "Set the logging level")

def train(train_dir, model, filenames):
  """Train the Sentiment model.
  
  Arguments:
    model: Sentiment model instance.

  Returns
    Nothing.
  """
  with tf.Graph().as_default():
    global_step = tf.contrib.framework.get_or_create_global_step()

    x, y = s_input.Pipeline(
        model.sequence_length,
    ).tfrecord_input(
        filenames,
        FLAGS.min_queue_examples,
        FLAGS.batch_size,
        num_threads=FLAGS.num_threads)
    logit_op = model.inference(x, dropout_prob=FLAGS.dropout_prob)
    accuracy_op = model.accuracy(model.prediction(logit_op), y)
    loss_op = model.loss(logit_op, y)
    train_op = model.train(loss_op, global_step)

    class _LoggerHook(tf.train.SessionRunHook):
      def begin(self):
        self._last_dump= time.time()
        self._writer = tf.summary.FileWriter(train_dir)
        self._embedding_var = tf.get_collection("embedding")[0]
        # Setup the embedding metadata for use with tensorboard.
        # https://www.tensorflow.org/how_tos/embedding_viz/
        from tensorflow.contrib.tensorboard.plugins import projector
        config = projector.ProjectorConfig()
        embedding = config.embeddings.add()
        embedding.tensor_name = self._embedding_var.op.name
        embedding.metadata_path = os.path.join(train_dir, "metadata")
        projector.visualize_embeddings(self._writer, config)
      def before_run(self, run_context):
        self._start_time = time.time()
        return tf.train.SessionRunArgs([global_step, self._embedding_var, loss_op, accuracy_op])
      def after_run(self, run_context, run_values):
        step, embedding, loss, accuracy = run_values.results
        if step % FLAGS.stat_interval == 0:
          duration = time.time() - self._start_time
          examples_per_sec = FLAGS.batch_size / duration
          sec_per_batch = float(duration)
          log.info("{}: step {:d} loss {:g} acc {:g} ({:g} e/s {:g} s/b)".format(
              datetime.now(), step, loss, accuracy, examples_per_sec, sec_per_batch))
        if FLAGS.dump_embedding and (time.time()-self._last_dump) >= FLAGS.save_checkpoint_secs:
          log.info("Dumping embedding matrix for {}".format(step))
          with open(os.path.join(train_dir, "embedding.tsv"), "w") as f:
            for row in embedding:
              f.write("\t".join(str(x) for x in row))
            f.write("\n")
          self._last_dump = time.time()

    with tf.train.MonitoredTrainingSession(
        checkpoint_dir=train_dir,
        save_checkpoint_secs=FLAGS.save_checkpoint_secs,
        save_summaries_steps=FLAGS.save_summaries_steps,
        hooks=[tf.train.StopAtStepHook(last_step=FLAGS.max_steps),
            tf.train.NanTensorHook(loss_op),
            _LoggerHook()],
        config=tf.ConfigProto(
            allow_soft_placement=FLAGS.allow_soft_placement,
            log_device_placement=FLAGS.log_device_placement)) as mon_sess:
      while not mon_sess.should_stop():
        mon_sess.run(train_op)

def main(argv=None):
  def _new_train_dir(train_dir, vocab_processor):
    """Creates and sets up the train directory

    Arguments:
      train_dir: Training directory.
      vocab_processor: Vocabulary processor.

    Returns:
      model: Configured model instance.
    """
    os.makedirs(train_dir)
    model = SentimentModel(
        sequence_length=vocab_processor.max_document_length,
        num_classes=FLAGS.num_classes,
        vocab_size=len(vocab_processor.vocabulary_),
        embedding_size=FLAGS.embedding_size,
        filter_sizes=list(map(int, FLAGS.filter_sizes.split(","))),
        num_filters=FLAGS.num_filters,
        l2_lambda=FLAGS.l2_lambda)
    model.save(os.path.join(train_dir, "model.pkl"))
    # As this is a fresh run we also need to store the metadata mapping.
    # NB: This is very finiky, so I'll be keeping both varients in source.
    #import csv
    #with open(os.path.join(train_dir, "metadata.tsv"), "w") as f:
        #fieldnames = ["Id", "Token"]
        #writer = csv.DictWriter(f, delimiter="\t", fieldnames=fieldnames)
        #writer.writeheader()
        #for token, token_id in vocab_processor.vocabulary_._mapping.items():
            #writer.writerow({"Id": token_id, "Token": token})
    with open(os.path.join(train_dir, "metadata"), "w") as f:
        for token in vocab_processor.vocabulary_._reverse_mapping:
            f.write("{}\n".format(token))
    return model
  class HelpArgumentParser(argparse.ArgumentParser):
    def error(self, message):
      sys.stderr.write("error: {}\n".format(message))
      self.print_help(sys.stderr)
      sys.exit(2)
  log.setLevel(FLAGS.log_level)
  parser = HelpArgumentParser(add_help=False)
  parser.add_argument(
      "data_dir",
      type=str,
      help="Path to data directory: /data/{vocab, train/, eval/}")
  args, _ = parser.parse_known_args()
  if not os.path.exists(args.data_dir):
    log.error("Data directory does not exist: {}".format(args.data_dir))
    return

  train_dir = os.path.join(FLAGS.run_dir, "train")
  if not os.path.exists(train_dir):
    # Load the vocab_processor that should exist within the data directory.
    vocab_processor = VocabularyProcessor.restore(os.path.join(args.data_dir, "vocab"))
    model = _new_train_dir(train_dir, vocab_processor)
  else:
    model = SentimentModel.restore(os.path.join(train_dir, "model.pkl"))
  # Fetch filenames from the train directory under data.
  filenames = glob.glob(os.path.join(args.data_dir, "train", "*.tfrecord"))

  train(train_dir, model, filenames)

if __name__ == "__main__":
  tf.app.run(main=main, argv=sys.argv)
