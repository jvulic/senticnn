numpy==1.11.3
pandas==0.19.2
protobuf==3.1.0.post1
python-dateutil==2.6.0
pytz==2016.10
six==1.10.0
tensorflow==0.12.1
